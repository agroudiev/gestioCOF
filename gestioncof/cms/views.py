from django.shortcuts import render

from gestioncof.cms.forms import CaptchaForm


def raw_calendar_view(request, year, month):
    return render(request, "cofcms/calendar_raw.html", {"month": month, "year": year})


def sympa_captcha_form_view(request):
    if request.method == "POST":
        form = CaptchaForm(request.POST)
        if form.is_valid():
            return render(
                request,
                "cofcms/sympa.html",
                {"redirect": "https://lists.ens.fr/wws/lists/"},
            )
    else:
        form = CaptchaForm()

    return render(request, "cofcms/sympa.html", {"form": form})
