#!/bin/sh

# Arête le script quand :
# - une erreur survient
# - on essaie d'utiliser une variable non définie
# - on essaie d'écraser un fichier avec une redirection (>).
set -euC

# Configuration de la base de données, redis, Django, etc.
# Tous les mots de passe sont constant et en clair dans le fichier car c'est
# pour une installation de dév locale qui ne sera accessible que depuis la
# machine virtuelle.
readonly DBUSER="cof_gestion"
readonly DBNAME="cof_gestion"
readonly DBPASSWD="4KZt3nGPLVeWSvtBZPSM3fSzXpzEU4"
readonly REDIS_PASSWD="dummy"
readonly DJANGO_SETTINGS_MODULE="gestioasso.settings.dev"


# ---
# Installation des paquets systèmes
# ---

get_packages_list () {
  sed 's/#.*$//' /vagrant/provisioning/packages.list | grep -v '^ *$'
}

# https://github.com/chef/bento/issues/661
export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y upgrade
get_packages_list | xargs apt-get install -y


# ---
# Configuration de la base de données
# ---

# Postgresql
pg_user_exists () {
  sudo -u postgres psql postgres -tAc \
    "SELECT 1 FROM pg_roles WHERE rolname='$1'" \
    | grep -q '^1$'
}

pg_db_exists () {
  sudo -u postgres psql postgres -tAc \
    "SELECT 1 FROM pg_database WHERE datname='$1'" \
    | grep -q '^1$'
}

pg_db_exists "$DBNAME" || sudo -u postgres createdb "$DBNAME"
pg_user_exists "$DBUSER" || sudo -u postgres createuser -SdR "$DBUSER"
sudo -u postgres psql -c "ALTER USER $DBUSER WITH PASSWORD '$DBPASSWD';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $DBNAME TO $DBUSER;"


# ---
# Configuration de redis (pour django-channels)
# ---

# Redis
redis-cli CONFIG SET requirepass "$REDIS_PASSWD"
redis-cli -a "$REDIS_PASSWD" CONFIG REWRITE


# ---
# Préparation de Django
# ---

# Dossiers pour le contenu statique
mkdir -p /srv/gestiocof/media
mkdir -p /srv/gestiocof/static
chown -R vagrant:www-data /srv/gestiocof

# Environnement virtuel python
sudo -H -u vagrant python3 -m venv ~vagrant/venv
sudo -H -u vagrant ~vagrant/venv/bin/pip install -U pip
sudo -H -u vagrant ~vagrant/venv/bin/pip install \
                     -r /vagrant/requirements-prod.txt \
                     -r /vagrant/requirements-devel.txt \

# Préparation de Django
cd /vagrant
ln -s -f secret_example.py gestioasso/settings/secret.py
sudo -H -u vagrant \
  DJANGO_SETTINGS_MODULE="$DJANGO_SETTINGS_MODULE"\
  /bin/sh -c ". ~vagrant/venv/bin/activate && /bin/sh provisioning/prepare_django.sh"
~vagrant/venv/bin/python manage.py collectstatic \
  --noinput \
  --settings "$DJANGO_SETTINGS_MODULE"


# ---
# Units systemd
# ---

# - Daphne fait tourner le serveur asgi
# - worker = https://channels.readthedocs.io/en/stable/topics/worker.html
# - Mails de rappels du BdA
# - Mails de revente du BdA
ln -sf /vagrant/provisioning/systemd/daphne.service   /etc/systemd/system/daphne.service
ln -sf /vagrant/provisioning/systemd/worker.service   /etc/systemd/system/worker.service
ln -sf /vagrant/provisioning/systemd/reventes.service /etc/systemd/system/reventes.service
ln -sf /vagrant/provisioning/systemd/rappels.service  /etc/systemd/system/rappels.service
ln -sf /vagrant/provisioning/systemd/reventes.timer   /etc/systemd/system/reventes.timer
ln -sf /vagrant/provisioning/systemd/rappels.timer    /etc/systemd/system/rappels.timer
systemctl enable --now daphne.service
systemctl enable --now worker.service
systemctl enable rappels.timer
systemctl enable reventes.timer


# ---
# Configuration du shell de l'utilisateur 'vagrant' pour utiliser le bon fichier
# de settings et et bon virtualenv.
# ---

# On utilise .bash_aliases au lieu de .bashrc pour ne pas écraser la
# configuration par défaut.
rm -f ~vagrant/.bash_aliases
cat > ~vagrant/.bash_aliases <<EOF
# On utilise la version de développement de GestioCOF
export DJANGO_SETTINGS_MODULE='$DJANGO_SETTINGS_MODULE'

# Charge le virtualenv
. ~/venv/bin/activate

# On va dans /vagrant où se trouve le code de gestioCOF
cd /vagrant
EOF


# ---
# Configuration d'nginx
# ---

ln -s -f /vagrant/provisioning/nginx/gestiocof.conf /etc/nginx/sites-enabled/gestiocof.conf
rm -f /etc/nginx/sites-enabled/default
systemctl reload nginx
