from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Club(models.Model):
    name = models.CharField(_("nom du club"), max_length=1000, unique=True)
    description = models.TextField(_("description"), blank=True)
    respos = models.ManyToManyField(
        User, verbose_name=_("responsables du club"), blank=True
    )

    def __str__(self):
        return self.name
