from urllib.parse import urlencode

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from shared import autocomplete

User = get_user_model()


class BDSMemberSearch(autocomplete.ModelSearch):
    model = User
    search_fields = ["username", "first_name", "last_name"]
    verbose_name = _("Membres du BDS")

    def get_queryset_filter(self, *args, **kwargs):
        qset_filter = super().get_queryset_filter(*args, **kwargs)
        qset_filter &= Q(bds__is_member=True)
        return qset_filter

    def result_uuid(self, user):
        return user.username

    def result_link(self, user):
        return reverse("bds:user.update", args=(user.pk,))


class BDSOthersSearch(autocomplete.ModelSearch):
    model = User
    search_fields = ["username", "first_name", "last_name"]
    verbose_name = _("Non-membres du BDS")

    def get_queryset_filter(self, *args, **kwargs):
        qset_filter = super().get_queryset_filter(*args, **kwargs)
        qset_filter &= Q(bds__isnull=True) | Q(bds__is_member=False)
        return qset_filter

    def result_uuid(self, user):
        return user.username

    def result_link(self, user):
        return reverse("bds:user.update", args=(user.pk,))


class BDSLDAPSearch(autocomplete.LDAPSearch):
    def result_link(self, clipper):
        url = reverse("bds:user.create.fromclipper", args=(clipper.clipper,))
        get = {"fullname": clipper.fullname, "mail": clipper.mail}

        return "{}?{}".format(url, urlencode(get))


class BDSSearch(autocomplete.Compose):
    search_units = [
        ("members", BDSMemberSearch()),
        ("others", BDSOthersSearch()),
        ("clippers", BDSLDAPSearch()),
    ]


bds_search = BDSSearch()
