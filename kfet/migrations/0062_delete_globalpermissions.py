# -*- coding: utf-8 -*-
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [("kfet", "0061_add_perms_config")]

    operations = [migrations.DeleteModel(name="GlobalPermissions")]
