{
  pkgs ? import <nixpkgs> { },
  ...
}:

let
  python = pkgs.python39;
in

pkgs.mkShell {
  shellHook = ''
    export DJANGO_SETTINGS_MODULE=gestioasso.settings.local

    virtualenv .venv
    source .venv/bin/activate

    pip install -r requirements-devel.txt | grep -v 'Requirement already satisfied:'
  '';

  packages =
    [ python ]
    ++ (with python.pkgs; [
      django-types
      pip
      virtualenv
      python-ldap
    ]);

  allowSubstitutes = false;
}
