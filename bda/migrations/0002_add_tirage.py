# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
from django.utils import timezone


def fill_tirage_fields(apps, schema_editor):
    """
    Create a `Tirage` to fill new field `tirage` of `Participant`
    and `Spectacle` already existing.
    """
    Participant = apps.get_model("bda", "Participant")
    Spectacle = apps.get_model("bda", "Spectacle")
    Tirage = apps.get_model("bda", "Tirage")

    # These querysets only contains instances not linked to any `Tirage`.
    participants = Participant.objects.filter(tirage=None)
    spectacles = Spectacle.objects.filter(tirage=None)

    if not participants.count() and not spectacles.count():
        # No need to create a "trash" tirage.
        return

    tirage = Tirage.objects.create(
        title="Tirage de test (migration)",
        active=False,
        ouverture=timezone.now(),
        fermeture=timezone.now(),
    )

    participants.update(tirage=tirage)
    spectacles.update(tirage=tirage)


class Migration(migrations.Migration):
    dependencies = [("bda", "0001_initial")]

    operations = [
        migrations.CreateModel(
            name="Tirage",
            fields=[
                (
                    "id",
                    models.AutoField(
                        verbose_name="ID",
                        serialize=False,
                        auto_created=True,
                        primary_key=True,
                    ),
                ),
                ("title", models.CharField(max_length=300, verbose_name=b"Titre")),
                (
                    "ouverture",
                    models.DateTimeField(
                        verbose_name=b"Date et heure d'ouverture du tirage"
                    ),
                ),
                (
                    "fermeture",
                    models.DateTimeField(
                        verbose_name=b"Date et heure de fermerture du tirage"
                    ),
                ),
                (
                    "token",
                    models.TextField(verbose_name=b"Graine du tirage", blank=True),
                ),
                (
                    "active",
                    models.BooleanField(default=True, verbose_name=b"Tirage actif"),
                ),
            ],
        ),
        migrations.AlterField(
            model_name="participant",
            name="user",
            field=models.ForeignKey(
                to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE
            ),
        ),
        # Create fields `spectacle` for `Participant` and `Spectacle` models.
        # These fields are not nullable, but we first create them as nullable
        # to give a default value for existing instances of these models.
        migrations.AddField(
            model_name="participant",
            name="tirage",
            field=models.ForeignKey(
                to="bda.Tirage", null=True, on_delete=models.CASCADE
            ),
        ),
        migrations.AddField(
            model_name="spectacle",
            name="tirage",
            field=models.ForeignKey(
                to="bda.Tirage", null=True, on_delete=models.CASCADE
            ),
        ),
        migrations.RunPython(fill_tirage_fields, migrations.RunPython.noop),
        migrations.AlterField(
            model_name="participant",
            name="tirage",
            field=models.ForeignKey(to="bda.Tirage", on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name="spectacle",
            name="tirage",
            field=models.ForeignKey(to="bda.Tirage", on_delete=models.CASCADE),
        ),
    ]
