"""
Settings par défaut et settings communs à GestioCOF et GestioBDS.
"""

import os
import sys

# ---
# Secrets
# ---

try:
    from . import secret
except ImportError:
    raise ImportError(
        "The secret.py file is missing.\n"
        "For a development environment, simply copy secret_example.py"
    )


def import_secret(name):
    """
    Shorthand for importing a value from the secret module and raising an
    informative exception if a secret is missing.
    """
    try:
        return getattr(secret, name)
    except AttributeError:
        raise RuntimeError("Secret missing: {}".format(name))


SECRET_KEY = import_secret("SECRET_KEY")
ADMINS = import_secret("ADMINS")
SERVER_EMAIL = import_secret("SERVER_EMAIL")
EMAIL_HOST = import_secret("EMAIL_HOST")

DBNAME = import_secret("DBNAME")
DBUSER = import_secret("DBUSER")
DBPASSWD = import_secret("DBPASSWD")

LDAP_SERVER_URL = import_secret("LDAP_SERVER_URL")


# ---
# Default Django settings
# ---

DEBUG = False  # False by default feels safer
TESTING = len(sys.argv) > 1 and sys.argv[1] == "test"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

INSTALLED_APPS = [
    "shared",
    # Must be before 'django.contrib.admin'.
    # https://django-autocomplete-light.readthedocs.io/en/master/install.html
    "dal",
    "dal_select2",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.admin",
    "django.contrib.admindocs",
    "gestioasso.apps.IgnoreSrcStaticFilesConfig",
    "django_cas_ng",
    "bootstrapform",
    "widget_tweaks",
]

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.security.SecurityMiddleware",
    "django.middleware.locale.LocaleMiddleware",
]

ROOT_URLCONF = "gestioasso.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
            ]
        },
    }
]

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DBNAME,
        "USER": DBUSER,
        "PASSWORD": DBPASSWD,
        "HOST": os.environ.get("DBHOST", "localhost"),
    }
}

SITE_ID = 1


# ---
# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
# ---

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_L10N = True
USE_TZ = True
LANGUAGES = (("fr", "Français"), ("en", "English"))
FORMAT_MODULE_PATH = "gestioasso.locale"


# ---
# Auth-related stuff
# ---

AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.ModelBackend"]

CAS_SERVER_URL = "https://cas.eleves.ens.fr/"
CAS_VERSION = "2"
CAS_LOGIN_MSG = None
CAS_IGNORE_REFERER = True
CAS_REDIRECT_URL = "/"
CAS_EMAIL_FORMAT = "%s@clipper.ens.fr"
