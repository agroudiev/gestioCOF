from django.contrib.staticfiles.apps import StaticFilesConfig


class IgnoreSrcStaticFilesConfig(StaticFilesConfig):
    ignore_patterns = StaticFilesConfig.ignore_patterns + ["src/**"]
