from django.urls import path

from . import views

urlpatterns = [
    path("raw_open", views.raw_open, name="kfet.open.edit_raw_open"),
    path("force_close", views.force_close, name="kfet.open.edit_force_close"),
]
