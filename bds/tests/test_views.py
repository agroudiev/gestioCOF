from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import Client, TestCase
from django.urls import reverse, reverse_lazy

User = get_user_model()


def give_bds_buro_permissions(user: User) -> None:
    perm = Permission.objects.get(content_type__app_label="bds", codename="is_team")
    user.user_permissions.add(perm)


def login_url(next=None):
    login_url = reverse_lazy(settings.LOGIN_URL)
    if next is None:
        return login_url
    else:
        return "{}?next={}".format(login_url, next)


class TestHomeView(TestCase):
    @mock.patch("gestioncof.signals.messages")
    def test_get(self, mock_messages):
        user = User.objects.create_user(username="random_user")
        give_bds_buro_permissions(user)
        self.client.force_login(
            user, backend="django.contrib.auth.backends.ModelBackend"
        )
        resp = self.client.get(reverse("bds:home"))
        self.assertEquals(resp.status_code, 200)


class TestRegistrationView(TestCase):
    @mock.patch("gestioncof.signals.messages")
    def test_get_autocomplete(self, mock_messages):
        user = User.objects.create_user(username="toto")
        url = reverse("bds:autocomplete") + "?q=foo"
        client = Client()

        # Anonymous GET
        resp = client.get(url)
        self.assertRedirects(resp, login_url(next=url))

        # Logged-in but unprivileged GET
        client.force_login(user, backend="django.contrib.auth.backends.ModelBackend")
        resp = client.get(url)
        self.assertEquals(resp.status_code, 403)

        # Burô user GET
        give_bds_buro_permissions(user)
        resp = client.get(url)
        self.assertEquals(resp.status_code, 200)

    @mock.patch("gestioncof.signals.messages")
    def test_get(self, mock_messages):
        user = User.objects.create_user(username="toto")
        url = reverse("bds:user.update", args=(user.id,))
        client = Client()

        # Anonymous GET
        resp = client.get(url)
        self.assertRedirects(resp, login_url(next=url))

        # Logged-in but unprivileged GET
        client.force_login(user, backend="django.contrib.auth.backends.ModelBackend")
        resp = client.get(url)
        self.assertEquals(resp.status_code, 403)

        # Burô user GET
        give_bds_buro_permissions(user)
        resp = client.get(url)
        self.assertEquals(resp.status_code, 200)
