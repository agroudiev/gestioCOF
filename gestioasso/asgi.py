import os

from channels.asgi import get_channel_layer

if "DJANGO_SETTINGS_MODULE" not in os.environ:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gestioasso.settings")

channel_layer = get_channel_layer()
