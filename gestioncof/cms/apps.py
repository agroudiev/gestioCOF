from django.apps import AppConfig


class COFCMSAppConfig(AppConfig):
    name = "gestioncof.cms"
    label = "cofcms"
    verbose_name = "CMS COF"
